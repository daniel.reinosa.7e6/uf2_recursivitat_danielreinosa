/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2023/01/07
* TITLE: 2.4 Nombres creixents
*/
import java.util.*
import javax.swing.ComponentInputMap
import javax.swing.ImageIcon

fun main() {
    val scanner = Scanner(System.`in`)
    println("Indica un numero")
    val input = scanner.nextInt()
    println(creixents(input))
}
fun creixents(input:Int):Boolean{
    if(input/10 >0)return input%10 > (input/10)%10 && creixents(input/10)
    else return true
}
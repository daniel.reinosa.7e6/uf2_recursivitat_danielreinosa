/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2023/01/07
* TITLE: 2.1 Factorial
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("Indica un numero")
    val input = scanner.nextInt()
    factorial(input)
    println(factorial(input))
}

fun factorial(input:Int):Int{
    if (input== 1 || input == 0) return 1
    else return input*factorial(input-1)
}
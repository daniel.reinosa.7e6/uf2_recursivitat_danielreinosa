/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2023/01/07
* TITLE: 2.2 Doble factorial
*/
import java.util.*

fun doblefactorial(input: Int):Int {
    if (input == 1 || input == 0) return 1
    else return input * doblefactorial(input-2)
}

fun main() {
    val scanner = Scanner(System.`in`)
    println("Indica un numero")
    val input = scanner.nextInt()
    println(doblefactorial(input))

}

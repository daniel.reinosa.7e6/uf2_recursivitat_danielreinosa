/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2023/01/07
* TITLE: 2.7 Primers perfectes
*/
import java.util.*
import javax.swing.ComponentInputMap
import javax.swing.ImageIcon

fun main() {
    val scanner = Scanner(System.`in`)
    println("Indica un numero")
    val input = scanner.nextInt()
    println(primerperfecte(input))
}
fun reduccioprime(input:Int):Int{
    return if(input/10 > 0)reduccioprime(sumaDigitsprime(input))
    else input
}
fun sumaDigitsprime(input: Int):Int{
    return if (input/10 >0)input%10 + sumaDigitsprime(input/10)
    else input
}
fun primerperfecte(number: Int): Boolean {
    for(i in 2 until reduccioprime(number)){
        if(reduccioprime(number)%i == 0)return false
    }
    return true
}

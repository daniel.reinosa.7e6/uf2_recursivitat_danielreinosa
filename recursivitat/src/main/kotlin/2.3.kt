/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2023/01/07
* TITLE: 2.3 Nombre de dígits
*/
import java.util.*
import javax.swing.ImageIcon

fun main() {
    val scanner = Scanner(System.`in`)
    println("Indica un numero")
    val input = scanner.nextInt()
    println(digits(input))
}
fun digits(input:Int):Int{
    if(input >0)return 1+digits(input/10)
    else return 0
}

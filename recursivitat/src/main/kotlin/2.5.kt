/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2023/01/07
* TITLE: 2.5 Reducció de dígits
*/
import java.util.*
import javax.swing.ComponentInputMap
import javax.swing.ImageIcon

fun main() {
    val scanner = Scanner(System.`in`)
    println("Indica un numero")
    val input = scanner.nextInt()
    println(reduccio(input))
}
fun reduccio(input:Int):Int{
    return if(input/10 > 0)reduccio(sumaDigits(input))
    else input
}
fun sumaDigits(input: Int):Int{
    return if (input/10 >0)input%10 + sumaDigits(input/10)
    else input
}